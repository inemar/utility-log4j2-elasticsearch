package net.inemar.utility.log4j2_elastic;

import static org.elasticsearch.common.settings.ImmutableSettings.settingsBuilder;

import java.io.Serializable;
import java.net.Inet4Address;
import java.net.URI;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.Filter;
import org.apache.logging.log4j.core.Layout;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.appender.AbstractAppender;
import org.apache.logging.log4j.core.config.plugins.Plugin;
import org.apache.logging.log4j.core.config.plugins.PluginAttribute;
import org.apache.logging.log4j.core.config.plugins.PluginElement;
import org.apache.logging.log4j.core.config.plugins.PluginFactory;
import org.apache.logging.log4j.message.MapMessage;
import org.elasticsearch.action.bulk.BulkRequestBuilder;
import org.elasticsearch.action.index.IndexRequestBuilder;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;

import com.google.gson.Gson;



@Plugin(name = "ElasticAppender", category = "Core", elementType = "appender", printObject = true)
public class ElasticAppender extends AbstractAppender {

	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1201362115669111175L;
	protected ElasticAppender(Config config, Filter filter,
			Layout<? extends Serializable> layout) {
		super(config.name, filter, layout);
		this.config=config;
		que=new ArrayBlockingQueue<InternalLogEvent>(config.buffer);
		this.level=config.level;
		LOGGER.info("Generated ElasticAppender "+config);
		// TODO Auto-generated constructor stub
	}
	
	
	static private class Config {
		String name="elastic";
		List<URI> uri=null;
		String cluster=null;
		String index="logstash";
		String type="log";
		RotateIndexType rotateIndexParam=RotateIndexType.DAY;
		String node="local";
		String service="java";
		int buffer=50000;
		long expiry=-1;
		Level level;

		@Override
		public String toString() {
			return "Config [name=" + name + ", uri=" + uri + ", cluster="
					+ cluster + ", index=" + index + ", type=" + type
					+ ", rotateIndexParam=" + rotateIndexParam + ", node="
					+ node + ", service=" + service + ", buffer=" + buffer
					+ ", level=" + level+ ", expiry=" + expiry + "]";
		}
	
	
	}
	
	private static String getProp(String param,String name,String def) {
		if (param!=null) {
			return param;
		}
		String str=null;
		str=System.getProperty(name);
		if (str!=null) {
			return str;
		};
		str=System.getenv(name);
		if (str!=null) {
			return str;
		};
		return def;		
	}
	
	
	 @PluginFactory
	 public static synchronized ElasticAppender createAppender(
			 	@PluginAttribute("name") String name,
			 	@PluginAttribute(value="uri") String elastic_local,
			 	@PluginAttribute(value="cluster") String elastic_cluster,
	            @PluginAttribute(value="index",defaultString="logstash") String index,
	            @PluginAttribute(value="indexRotate",defaultString="DAY") String indexRotate,
	            @PluginAttribute(value="type",defaultString="logs") String type,
	            @PluginAttribute(value="node") String node,
	            @PluginAttribute(value="service") String service,
			 	@PluginAttribute(value="bufferSize",defaultInt=5000) Integer bufferSize,
			 	@PluginAttribute(value="expiryTime",defaultLong=-1) Long expiry,
			 	@PluginAttribute(value="expiryUnit",defaultString="d") String expiryUnit,
	            @PluginAttribute("level") Level level,
	            @PluginAttribute("levelin") Level levelin,
	            @PluginElement("Filters") Filter filter) {
		 
		 	LOGGER.info("Create new Elastic Appender Version 3.1.0");
		 	
		 	Config config=new Config();
		 	if (name == null) {
		            LOGGER.error("No name provided for StubAppender");
		            return null;
		    }
		 	config.name=name;
		 	String myhost="Unknown";
	        try {
				myhost=Inet4Address.getLocalHost().getHostAddress().toString();
			} catch (UnknownHostException e) {
				;
			}
	        config.node=getProp(node,"node",myhost);
			config.service=getProp(service,"service","Java");
			config.cluster=getProp(elastic_cluster,"elastic_cluster","elasticsearch");
			elastic_local=getProp(elastic_local,"elastic_local","native://localhost:9300");
	        
	        String [] uriDat=elastic_local.split("\\,");
	        config.uri=new LinkedList<URI>();
	        for(String act:uriDat) {
	        	LOGGER.debug("Found following URI "+act);
	        	config.uri.add(URI.create(act));
	        }
	        config.rotateIndexParam=RotateIndexType.NO;
	        if (indexRotate==null) {
	        	config.rotateIndexParam=RotateIndexType.DAY;
	        } else if (indexRotate.equalsIgnoreCase("NO")) {
	        	config.rotateIndexParam=RotateIndexType.NO;		        	
	        } else if (indexRotate.equalsIgnoreCase("DAY")) {
	        	config.rotateIndexParam=RotateIndexType.DAY;		        	
	        } else if (indexRotate.equalsIgnoreCase("HOUR")) {
	        	config.rotateIndexParam=RotateIndexType.HOUR;		        	
	        } else {
	        	LOGGER.warn("Illegal type for indexRotate only support NO,DAY,HOUR - default to DAY");;
	        	config.rotateIndexParam=RotateIndexType.DAY;
	        };
	        
	        if (level==null) {
	        	config.level=Level.ALL;
	        } else {
	        	config.level=level;
	        }
	        if (index!=null) {
	        	config.index=index;
	        }
	        if (type!=null) {
	        	config.type=type;
	        };
	        if (bufferSize!=null) {
	        	config.buffer=bufferSize;
	        }
	        long factor=24*60*60*1000L;
	        if (expiryUnit!=null) {
	        	if (expiryUnit.equalsIgnoreCase("w")) {
	        		factor=7*24*60*60*1000L;
	        	} else if (expiryUnit.equalsIgnoreCase("d")) {
	        		factor=24*60*60*1000L;
	        	} else if (expiryUnit.equalsIgnoreCase("h")) {
	        		factor=60*60*1000L;
	        	} else if (expiryUnit.equalsIgnoreCase("m")) {
	        		factor=60*1000L;
	        	} else if (expiryUnit.equalsIgnoreCase("s")) {
	        		factor=1000L;
	        	} else if (expiryUnit.equalsIgnoreCase("msec")) {
	        		factor=1000L;
	        	};
	        } 
	        if (expiry!=null) {
	        	if (expiry>0) {
	        		config.expiry=expiry*factor;
	        	};
	        };
		 	
		 	return new ElasticAppender(config,filter,null);
	 };
	 
	 
	 

	 
	volatile Level level=Level.ALL; 
	//volatile long lastWarn=0;
	volatile Map<String,Level> special=new HashMap<String,Level>();
	
	
	static class InternalLogEvent {
		String threadName="";
		LogEvent eve;
		
		InternalLogEvent(LogEvent event) {
			eve=event;
			threadName=eve.getThreadName();
		}
	}
	
	ArrayBlockingQueue<InternalLogEvent> que=null;
	@Override
	public void append(LogEvent event) {
		Level spec=special.get(event.getLoggerName());
		if (spec!=null) {
			if (event.getLevel().isMoreSpecificThan(spec)==false) {
				return;
			};
		} else {
			if (event.getLevel().isMoreSpecificThan(level)==false) {
				return;
			};
		};
		que.offer(new InternalLogEvent(event));		
	}
	

	volatile Config config=null;
	
	volatile boolean running=true;
	public void start() {
		running=true;
		Thread thr=new Thread() {
			public void run() {
				worker();
			}
		};
		thr.setDaemon(true);
		thr.setName("ElasticAppender "+config.name);
		thr.start();
		setStarted();
	}

	
	public void stop() {
		running=false;
	}

	
	void worker() {
        Settings settings = settingsBuilder()
                .put("cluster.name", config.cluster)
                .put("network.server", false)
                .put("node.client", true)
                .put("client.transport.sniff", false)
                .put("client.transport.ping_timeout", "30s")
                .put("client.transport.ignore_cluster_name", false)
                .put("client.transport.nodes_sampler_interval", "30s")
                .build();
        
        

        Logger logger=LogManager.getLogger("ElasticAppender "+config.name);
        logger.info("Start now ElasticAppender Thread");
        
        
    	String resolvedIndex=null;
    	long indexHour=0;
    	SimpleDateFormat formatHour=new SimpleDateFormat("yyyy.MM.dd.HH");;
    	SimpleDateFormat formatDay=new SimpleDateFormat("yyyy.MM.dd");;
    	
    	SimpleDateFormat formatElastic=new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");

    	Map<String,Object> eve=new HashMap<String,Object>();
        
		while(running) {
			TransportClient client=new TransportClient(settings,false);
	        for(URI act:config.uri) {
	        	client.addTransportAddress(new InetSocketTransportAddress(act.getHost(), act.getPort()));
	        };
	        if (client.connectedNodes().isEmpty()) {
	            client.close();
	            logger.error("unable to connect to Elasticsearch cluster");
	            try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					//e.printStackTrace();
				}
	            continue;
	        };
	        /*boolean syncready=true;
	        
	        //now check config
	        if (new IndicesExistsRequestBuilder(client.admin().indices(),LOGCONFIG).get().isExists()==false) {
	        	LOGGER.info("Index not existing create index");
	        	if (new CreateIndexRequestBuilder(client.admin().indices(),LOGCONFIG).get().isAcknowledged()==false) {
	        		LOGGER.error("Failure creating index");
	        		syncready=false;
	        	}
	        };
	        if (syncready) {
	        	if (new TypesExistsRequestBuilder(client.admin().indices(),LOGCONFIG).setTypes(LOGTYPE_MAIN,LOGTYPE_FILTER).get().isExists()==false) {
	        		LOGGER.info("Create now types");
		        	new DAO_Filter().write(client,gson);
		        	new DAO_MainConfig().write(client,gson);
	        	}
	    	        
	        }
	        if (syncready) {
	            GetResponse resp=new GetRequestBuilder(client)
	        	.setIndex(LOGCONFIG)
	        	.setType(LOGTYPE_MAIN)
	        	.setId(config.node+"-"+config.service).get();
	            if (!resp.isExists()) {
	            	ret.main=new DAO_MainConfig(node,service,Level.WARN.name());
	            	ret.main.write(client);
	        } else {
	        	ret.main=gson.fromJson(resp.getSourceAsString(), DAO_MainConfig.class);
	        };

	        }
	        */
	        
	        while(running) {
	        	InternalLogEvent ilog=null;
				try {
					ilog = que.poll(20, TimeUnit.SECONDS);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					//e.printStackTrace();
				}
	        	if (ilog==null) {
	        		continue;
	        	};
	        	List<InternalLogEvent> logs=new LinkedList<InternalLogEvent>();
	        	logs.add(ilog);
	        	que.drainTo(logs,500);
	        	BulkRequestBuilder bulkRequest = client.prepareBulk();
	        	for(InternalLogEvent ievent:logs) {
	        		LogEvent event=ievent.eve;
	        		if ((event.getTimeMillis()/3600000L)!=indexHour) {
	        			indexHour=event.getTimeMillis()/3600000L;
	        			switch (config.rotateIndexParam) {
	        			case HOUR: {
	        				long localTime=event.getTimeMillis();
	        				Date dd=new Date(localTime-TimeZone.getDefault().getOffset(localTime));
	        				resolvedIndex=config.index+"-"+formatHour.format(dd);
	        				break;
	        			}
	        			case DAY: {
	        				long localTime=event.getTimeMillis();
	        				Date dd=new Date(localTime-TimeZone.getDefault().getOffset(localTime));
	        				resolvedIndex=config.index+"-"+formatDay.format(dd);
	        				break;
	        			}
	        			case NO: {
	        				resolvedIndex=config.index;
	        				break;
	        			}
	        			}
	        		}
	        		//LOGGER.debug("Write to index "+resolvedIndex);
	        		
	        		eve.clear();
	        		if (event.getMessage() instanceof MapMessage) {
	        			eve.putAll(((MapMessage)event.getMessage()).getData());
	        		} else {
	        			eve.put("message",event.getMessage().getFormattedMessage());			
	        		}
	        		eve.put("@source",config.service);
	        		eve.put("host",config.node);
	        		eve.put("@version",1);
	        		eve.put("@timestamp",formatElastic.format(new Date(event.getTimeMillis())));
	        		eve.put("level",event.getLevel().toString());
	        		eve.put("logger",event.getLoggerName());
	        		eve.put("loggerFQDN",event.getLoggerFqcn());
	        		if (event.getMarker()!=null) {
	        			eve.put("marker",event.getMarker().toString());
	        		};
	        		eve.put("thread",ievent.threadName);
	        		if (event.getSource()!=null) {
	        			eve.put("stack",event.getSource().toString());
	        		};
	        		if (event.getThrown()!=null) {
	        			eve.put("throw",convThrowable(event.getThrown()));
	        		};
	        		eve.put("context",event.getContextMap());
	        		IndexRequestBuilder d=		client.prepareIndex(resolvedIndex, config.type).setSource(eve);
	        		if (config.expiry>0) {        			
	        			d.setTTL(config.expiry);
	        		}
	        		bulkRequest.add(d);

	        	}
	        	bulkRequest.execute();
	        }
		}
		setStopped(); 
	}
	
	
	
		
	
	private String convThrowable(Throwable t) {
		StringBuilder result = new StringBuilder();
	    result.append(t.toString());
	    result.append('\n');

	    //add each element of the stack trace
	    for (StackTraceElement element : t.getStackTrace()){
	      result.append(element);
	      result.append('\n');
	    }
	    result.append('\n');	   
	    if (t.getCause()!=null) {
	    	result.append("Caused by ...\n");
	    	result.append(convThrowable(t.getCause()));
	    	
	    }
	    return result.toString();
	}
	
	final static private String LOGCONFIG="log3config";
	final static private String LOGTYPE_MAIN="main";
	final static private String LOGTYPE_FILTER="filter";


	
	private static abstract class DAO {
		void write(TransportClient client,Gson gson) {
			new IndexRequestBuilder(client)
    		.setIndex(LOGCONFIG)
    		.setType(getType())
    		.setId(this.getId())
    		.setSource(gson.toJson(this))
    		.get();
		}
		
		
		
		
		protected abstract String getType();
		protected abstract String getId();
	}
	
	private static class DAO_Filter extends DAO {
		
		protected String getType() {
			return LOGTYPE_FILTER;
		}

		
		private DAO_Filter() {
			this.node="dummy";
			this.service="dummy";
			this.name="dummy";
			this.level=Level.ALL.name();
		}


		private DAO_Filter(String node, String service, String name, String level) {
			this.node = node;
			this.service = service;
			this.name = name;
			this.level = level;
		}

		

		
		String node;
		String service;
		String name;
		String level;	
		
		
		protected String getId() {
			return node+"-"+service+"-"+name;
		}
		
		

	}
		
	
	
	private static class DAO_MainConfig extends DAO {
		
		protected String getType() {
			return LOGTYPE_MAIN;
		}

		
		DAO_MainConfig() {
			this.node="dummy";
			this.service="dummy";
			this.defaultLevel=Level.ALL.name();
		}
		

		private DAO_MainConfig(String node, String service, String defaultLevel) {
			this.node = node;
			this.service = service;
			this.defaultLevel = defaultLevel;
		}
		String node;
		String service;
		String defaultLevel;
		String inLevel;
		int refresh=60;
		
		protected String getId() {
			return node+"-"+service;
		}
	}	
	
}
