# Log4j2 storage of data in Elasticsearch #

Elasticsearch (http://www.elasticsearch.org/) with a Kibana (http://www.elasticsearch.org/overview/kibana/) frontend is probably the absolute best way to access log files.

The aim of the project is to store java log data generated with log4j2 (http://logging.apache.org/log4j/2.x/) in an elasticsearch database. It also provides the possiblity to change 
the log settings using a web based configuration


## What is this repository for? ##

* Log4j2 Appender for Elasticsearch
* Web based managment of Log4j2 Levels


## Installation ##

The easiest way is to include the maven artifact:
```
     <groupId>net.inemar.utility</groupId>
     <artifactId>log4j2elastic</artifactId>
     <version>2.1.x</version>
```

This installs the following items:

* ElasticAppender - stores logs in ElasticSearch
* ElasticFilter  - filter configurable via ElasticSearch
* FluidMapMessage - a MapMessage extension for logging with an Fluid interface
* ElasticSync - applying config from ElasticSearch to logger, appender, filter


Add to your project a file with the name **log4j2.xml** and the following content:

```
<?xml version="1.0" encoding="UTF-8"?>
<Configuration status="WARN">
  <Appenders>
    <Console name="Console" target="SYSTEM_OUT">    
      <PatternLayout pattern="%d{HH:mm:ss.SSS} [%t] %-5level %logger{36} - %msg%n"/>
    </Console>
    <ElasticAppender name="ElasticAppender" manager="true"/>
  </Appenders>
  <Loggers>
    <Root level="all">
      <AppenderRef ref="Console">
      		<ElasticFilter name="ConsoleFilter" level="ERROR"/>    
      </AppenderRef>
      <AppenderRef ref="ElasticAppender"/>      
    </Root>    
  </Loggers>
</Configuration>
```

Get the [logmanager.zip](https://bitbucket.org/inemar/utility-log4j2-elasticsearch/src/de696bca49990a2fecf1c6843e9bc95705eb2f1a/html/logmanager.zip?at=Version_2.0) unzip it.
configure your url in config.js or configure it on the webpage directly.

deploy logmanager on a webserver or use it locally




## Configuration ElasticAppender ##

To configure a new ElasticAppender use the following minimal snippet
```
<ElasticAppender name="ElasticAppender"/>

```

This uses default parameters. The following table shows the actual parameter, which can be used in the appender
```
+------------------+-------------------------+-----------------------------------------------------------+
|    Parameter     |         Default         |                          Remark                           |
+------------------+-------------------------+-----------------------------------------------------------+
| name             |                         | mandatory parameter                                       |
| uri              | native://localhost:9300 | ip and port of elasticsearch database                     |
| cluster          | elasticsearch           | cluster name                                              |
| index            | logstash                | Specifies the index used for storing log records          |
| indexRotate      | DAY                     | NO or DAY or HOUR, auto rotates the index                 |
| type             | logs                    | the elastic search type used for storage of records       |
| node             | <host ip>               | used to identify the host                                 |
| service          | Java                    | used to identify the program                              |
| bufferSize       | 5000                    | number of records stored before sending to elastic search |
| flushSeconds     | 600                     | flush records to database every seconds                   |
| manager          | false                   | if this database connection shall be used for managment   |
| ignoreExceptions | false                   | if exceptions shall be ignored                            |
| level            | ALL                     | additional filter for Appender                            |
| Filters          |                         | additional filters                                        |
+------------------+-------------------------+-----------------------------------------------------------+
```

for certain parameters additional property lookup is performed

### uri ###

Evaluates in the following order

1. uri configured in log4j2 configuration
2. the java system property **elastic_local**
3. the enviroment variable **elastic_local**
4. the default value **native://localhost:9300**

multiple ip:port combinations can be specified by delimiting with ,

example: native://localhost:9300,native://127.0.0.1:9301

### cluster ###

Evaluates in the following order

1. cluster configured in log4j2 configuration
2. the java system property **elastic_cluster**
3. the enviroment variable **elastic_cluster**
4. the default value **elasticsearch**
 

### node ###

Evaluates in the following order

1. node configured in log4j2 configuration
2. the java system property **node**
3. the enviroment variable **node**
4. the local ip

### service ###

Evaluates in the following order

1. service configured in log4j2 configuration
2. the java system property **service**
3. the enviroment variable **service**
4. the default value **Java**


### manager ###

This should occur only once in the log4j2 configuration. If **manager=true** is multiple time in the config, only this first parsed  occurence is used.
Using this connection the elaticsearch database is connected for real-time config of the logging.

the following parameters can be configured via elastic search

* Filter Level of the special filter ElasticFilter
* Level of Elastic Appender
* Level of each Logger

## Configuration ElasticFilter ##

ElasticFilter provides a filter, where the filter level can be configured using elasticsearch database

The filter has the following parameters
```
+-----------+---------+---------------------+
| Parameter | Default |       Remark        |
+-----------+---------+---------------------+
| name      |         | mandatory parameter |
| level     | WARN    | filter level        |
+-----------+---------+---------------------+
```


## Using Fluid Map Message

Elastic Appender stores log messages normally in the message field.

If the log record uses a MapMessage type of message all the Map fields are stored as columns

To easily create MapMessage the following extension is included

```
private static Logger logger = LogManager.getLogger();
logger.error(
   ELM.msg("this is the message")
   .add("column1","this is stored as column1")
   .add("another_column","and another column")
);
```


## Example ##

The following example can be used as an blueprint for using all features
```
<?xml version="1.0" encoding="UTF-8"?>
<Configuration status="WARN">
  <Appenders>
    <Console name="Console" target="SYSTEM_OUT">    
      <PatternLayout pattern="%d{HH:mm:ss.SSS} [%t] %-5level %logger{36} - %msg%n"/>
    </Console>
    <ElasticAppender name="ElasticAppender" manager="true"/>
  </Appenders>
  <Loggers>
    <Root level="all">
      <AppenderRef ref="Console">
      		<ElasticFilter name="ConsoleFilter" level="ERROR"/>    
      </AppenderRef>
      <AppenderRef ref="ElasticAppender"/>      
    </Root>    
  </Loggers>
</Configuration>
```

## Dependencies ##

 This depends on 
 
 * log4j2
 * log4j2 core
 * google gson
 * elastic search
 
 
 ```
      <dependency>
            <groupId>org.apache.logging.log4j</groupId>
            <artifactId>log4j-core</artifactId>
            <version>${log4j2.version}</version>
        </dependency>

        <dependency>
            <groupId>org.elasticsearch</groupId>
            <artifactId>elasticsearch</artifactId>
            <version>${elasticsearch.version}</version>
            <type>jar</type>
            <scope>compile</scope>
        </dependency>  
        
        <dependency>
        	<groupId>com.google.code.gson</groupId>
        	<artifactId>gson</artifactId>
        	<version>2.2.4</version>
        </dependency>
        
        <dependency>
        	<groupId>org.apache.logging.log4j</groupId>
        	<artifactId>log4j-slf4j-impl</artifactId>
        	<version>2.0.2</version>
        </dependency>
```


## Web based Config of log4j2

Deploy the [logmanager.zip](https://bitbucket.org/inemar/utility-log4j2-elasticsearch/src/de696bca49990a2fecf1c6843e9bc95705eb2f1a/html/logmanager.zip?at=Version_2.0) either on a webserver or unzip it

in the config.js configure the url to the elastic search engine


## Database configuration ##

No database configuration is necessary


### Who do I talk to? ###

* you can contact me at opensource @ inemar.net
